# Transaccional

Es una solucón temporal

## Software requerido
- Java JRE o JDK 1.8 
- Agregar en la variable de ambiente PATH la ruta de bin\java del JDK o JRE 
-	Contar con permisos y accesos a los servidores de Base de Datos TPATxHistory y TPCentralDB 

## Requisitos mínimos del Sistema para Instalar
- Procesador: Procesador a 1 GHz
- RAM: 2 GB para 32 bits o 3 GB para 64 bits
- Espacio en disco duro: 16 GB 
- Conexión a red o VPN San Pablo
- Pantalla: 800x600

# Como instalar la aplicación 
1.	Crear una carpeta o folder 
2.	Copiar los archivos en la carpeta que creo en el paso anterior
	-	Transaccional.jar
	-	KeyGen.jar
	-	Transaccional.json
	-	Privada.dat
	-	Key.dat
3.	Crear un acceso directo a Transaccional.jar y KeyGen.jar
4.	Dar doble clic a los accesos directos para que se ejecuten las aplicaciones y pueda comprobar que se puedan ejecutar. En caso de que no se ejecute las causas posibles es versión equivocada de JDK o JRE, o en la variable de ambiente no esté definido la ruta de  java.exe


A. General. Configura la conexión de la base de datos. Cuenta con los atributos 

| Campo	 | Descripción	| Tipo Campo	| Obligatorio |
|---|---|---|---|
|llavePrivada	. | Ubicación de la llave privada generados por la aplicación KeyGen. Si la llave privada se encuentra en la misma carpeta donde está la aplicación Transaccional solo se deberá de indicar el nombre del archivo.	| String que define la ruta del archivo físico.	| Sí |
|llavePrivada	|Ubicación de la llave privada generados por la aplicación KeyGen. Si la llave privada se encuentra en la misma carpeta donde está la aplicación Transaccional solo se deberá de indicar el nombre del archivo.	|String que define la ruta del archivo físico.	|Sí|
|archivoKey	|Ubicación de la llave generada por la aplicación KeyGen. Si la llave se encuentra en la misma carpeta donde está la aplicación Transaccional solo se deberá de indicar el nombre del archivo.	|String que define la ruta del archivo físico.	|Sí|
|url	|IP o nombre del servidor de base de datos	|String. Conexión a base de datos	|Sí|
|Puerto	|Puerto utilizado por el manejador de Base de Datos SQL Server|Integer. Conexión a base de datos.	|Sí|
|baseDatos	|Nombre de la base de datos a utilizar en los reportes antes mencionados	|String. Conexión a base de datos	|Sí|
|numeroTransacciones	|El rango de valores es de 1 a diez millones, son el numero de transacciones que serán procesadas como un bloque, dependiendo de las capacidades de computo que tenga el equipo es el valor que deberá de configurar donde el valor de 1 es el del menor performance y entre mayor sea el número mayor será el desempeño en la transferencia de la información.	Integer. |Numero de consultas procesadas para  	|Sí|

B.	Farmacias

| Campo	 | Descripción	| Tipo Campo	| Obligatorio |
|---|---|---|---|
|id	|Identificador único de la farmacia, utilizando su llave primaria. Sera utilizado para realizar las consultas	|Integer. identificador	|Sí|
|descripcion	|Texto que vera el usuario para seleccionar la farmacia a consultar. 	|String. Nombre o descripción de la farmacia	|Sí|

C.	Tipo detalle

| Campo	 | Descripción	| Tipo Campo	| Obligatorio |
|---|---|---|---|
|id	|Identificador único del tipo detalle, utilizando su llave primaria. Sera utilizado para realizar las consultas	|Integer. identificador	|Sí|
|descripcion	|Texto que vera el usuario para seleccionar el tipo detalle a consultar. 	|String. Nombre o descripción del tipo detalle	|Sí|


## Utilizando la Consulta

 
1.	Seleccione tipo de reporte. Reporte del día actual o Reporte Histórico, si selecciona Reporte Histórico deberá de indicar la fecha inicial y fecha final.
2.	Seleccionar una o más farmacias. 
3.	Seleccionar un o mas tipos de detalles. 
Para seleccionar todas las farmacias o tipo detalle puede utilizar el botón
 
4.	Seleccione uno o ambos tipos de transacciones 
5.	Clic en el botón “Generar Reporte”
6.	Deberá indicar el nombre del archivo y en donde será generado
7.	Si la consulta que se esta realizando no genera información, el archivo de salida no va ser generado. 

