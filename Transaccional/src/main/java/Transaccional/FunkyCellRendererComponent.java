package Transaccional;
/**
* @Autthor Ramses Arellano Rincon
*/
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;

public class FunkyCellRendererComponent extends JPanel {

	private JCheckBox checkBox = new JCheckBox();
	private JLabel label = new JLabel();
	private JList list;
	private int index;

	public FunkyCellRendererComponent() {
		super(null);
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(this.checkBox);
		add(this.label);
	}

	public void setup(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		this.list = list;
		this.index = index;
		this.label.setText((String) list.getModel().getElementAt(index));
		if (isSelected) {
			setForeground(list.getSelectionForeground());
			setBackground(list.getSelectionBackground());
		} else {
			setForeground(list.getForeground());
			setBackground(list.getBackground());
		}
		this.checkBox.setSelected(isSelected);
	}

}
