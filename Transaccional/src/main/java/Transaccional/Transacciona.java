package Transaccional;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.apache.commons.lang3.SerializationUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.toedter.calendar.JDateChooser;

import Transaccional.modelos.list.checkbox.CheckBoxListRenderer;
import Transaccional.modelos.list.checkbox.CheckboxListItem;
import Transaccional.modelos.settings.Farmacia;
import Transaccional.modelos.settings.General;
import Transaccional.modelos.settings.Remota;
import Transaccional.modelos.settings.TipoDetalle;
import Transaccional.modelos.settings.ToSetting;
import Transaccional.modelos.sqlserver.Consultas;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * 
 * @author Ramses Arellano Rincon
 */
public class Transacciona {

	private JFrame frmGenerador;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	private final ButtonGroup buttonGroup_2 = new ButtonGroup();

	private JList lstTransaccionalFarmacias = null;
	private JList lstInstitucionesFarmacias = null;
	private JDateChooser dchTransaccionalFInicio;
	private JDateChooser dchTransaccionalFFinal;
	private JDateChooser dchpagaTodoFInicio;
	private JDateChooser dchpagaTodoFFinal;
	private JDateChooser dchCreditoFInicio;
	private JDateChooser dchCreditoFFinal;
	private JDateChooser dchDevFInicial;
	private JDateChooser dchDevFFinal;
	private JScrollPane scrollInstitucionesFarmacias;
	private JButton btnInstitucionSelecciona;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
					Transacciona window = new Transacciona();
					window.frmGenerador.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Transacciona() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		//genera();
		ToSetting toSettings = leeSeettings();
		
		if(toSettings==null) return ;

		CheckboxListItem[] items = leeCatalogFarmacia(toSettings, false);
		CheckboxListItem[] pagaTodoFarmacias = SerializationUtils.clone(items);
		CheckboxListItem[] institucionesFarmacias = SerializationUtils.clone(items);
		CheckboxListItem[] reporteCreditoFarmacias = SerializationUtils.clone(items);

		CheckboxListItem[] transaccionalTipoDetalle = leeCatalogTipoDetalle(toSettings);
		CheckboxListItem[] pagaTodolTipoDetalle = SerializationUtils.clone(transaccionalTipoDetalle);

		CheckboxListItem[] institucionesFarmaciasCbx = leeCatalogFarmacia(toSettings, true);
		CheckboxListItem[] devolucionFarmaciasCbx = SerializationUtils.clone(institucionesFarmaciasCbx);

		frmGenerador = new JFrame();
		frmGenerador.setResizable(false);
		frmGenerador.setTitle("Generador de Archivo Transaccional - Inteligencia de Negocios\r\n");
		frmGenerador.setBounds(100, 100, 928, 511);
		frmGenerador.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGenerador.getContentPane().setLayout(null);


		frmGenerador.setIconImage(
				(new ImageIcon(getClass().getResource("/Transaccional/imagenes/sanpablo3.png"))).getImage());

		JTabbedPane tbpPrincipal = new JTabbedPane(JTabbedPane.TOP);
		tbpPrincipal.setBounds(10, 10, 909, 467);
		frmGenerador.getContentPane().add(tbpPrincipal);

		try {
			DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		JPanel pnlTransaccional = new JPanel();
		tbpPrincipal.addTab("Reporte Transaccional", null, pnlTransaccional, null);
		pnlTransaccional.setLayout(null);

		JLabel lblTransaccionalHeader = new JLabel(
				"A continuación seleccione de entre las opciones disponibles la correspondiente al reporte requerido");
		lblTransaccionalHeader.setBounds(24, 30, 504, 13);
		pnlTransaccional.add(lblTransaccionalHeader);

		JPanel pnlTransaccionalTipoReporte = new JPanel();
		pnlTransaccionalTipoReporte.setLayout(null);
		pnlTransaccionalTipoReporte.setToolTipText("Seleccione un tipo de reporte");
		pnlTransaccionalTipoReporte.setBorder(new TitledBorder(null, "TIpo de Reporte ", TitledBorder.LEFT,
				TitledBorder.TOP, null, new Color(59, 59, 59)));
		pnlTransaccionalTipoReporte.setBounds(18, 41, 866, 61);
		pnlTransaccional.add(pnlTransaccionalTipoReporte);

		JRadioButton rbtnTransaccionalActual = new JRadioButton("Reporte del Día Actual\r\n ");
		rbtnTransaccionalActual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rbtnTransaccionalActual.isEnabled())
					inicializaFechaTransaccional(false);
			}
		});
		rbtnTransaccionalActual.setSelected(true);
		buttonGroup.add(rbtnTransaccionalActual);
		rbtnTransaccionalActual.setBounds(175, 19, 178, 21);
		pnlTransaccionalTipoReporte.add(rbtnTransaccionalActual);

		JRadioButton rbtnHistorico = new JRadioButton("Reporte Histórico");
		rbtnHistorico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rbtnHistorico.isEnabled())
					inicializaFechaTransaccional(true);
			}
		});

		buttonGroup.add(rbtnHistorico);
		rbtnHistorico.setBounds(580, 19, 155, 21);
		pnlTransaccionalTipoReporte.add(rbtnHistorico);

		JScrollPane scrollTransaccionalFarmacias = new JScrollPane();
		scrollTransaccionalFarmacias.setBounds(22, 134, 192, 250);
		pnlTransaccional.add(scrollTransaccionalFarmacias);


		lstTransaccionalFarmacias = new JList(items);
		lstTransaccionalFarmacias.setToolTipText("Farmacias a ser consultadas");
		scrollTransaccionalFarmacias.setViewportView(lstTransaccionalFarmacias);

		lstTransaccionalFarmacias.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList<CheckboxListItem> list = (JList<CheckboxListItem>) event.getSource();

				int index = list.locationToIndex(event.getPoint());
				CheckboxListItem item = (CheckboxListItem) list.getModel().getElementAt(index);

				item.setSelected(!item.isSelected());

				list.repaint(list.getCellBounds(index, index));
			}
		});

		lstTransaccionalFarmacias.setCellRenderer(new CheckBoxListRenderer());


		JButton btnTransaccionalSelecTodoFarmacias = new JButton("Seleccionar Todo");
		btnTransaccionalSelecTodoFarmacias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				actualizaStatusList(items, btnTransaccionalSelecTodoFarmacias, lstTransaccionalFarmacias);

			}
		});
		btnTransaccionalSelecTodoFarmacias.setBounds(22, 385, 192, 41);

		pnlTransaccional.add(btnTransaccionalSelecTodoFarmacias);

		JLabel lblTransaccionalFarmacias = new JLabel("Farmacias Seleccionadas:");
		lblTransaccionalFarmacias.setBounds(24, 114, 156, 13);
		pnlTransaccional.add(lblTransaccionalFarmacias);

		JLabel lblTipoDetalle = new JLabel("Tipo Detalle:");
		lblTipoDetalle.setBounds(301, 114, 156, 13);
		pnlTransaccional.add(lblTipoDetalle);


		JScrollPane scrollTransaccionalTipoDetalle = new JScrollPane();
		scrollTransaccionalTipoDetalle.setBounds(301, 134, 192, 250);
		pnlTransaccional.add(scrollTransaccionalTipoDetalle);

		JList lstTransaccionalTipoDetalle = new JList(transaccionalTipoDetalle);
		scrollTransaccionalTipoDetalle.setViewportView(lstTransaccionalTipoDetalle);

		lstTransaccionalTipoDetalle.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList<CheckboxListItem> list = (JList<CheckboxListItem>) event.getSource();

				// Get index of item clicked
				int index = list.locationToIndex(event.getPoint());
				CheckboxListItem item = (CheckboxListItem) list.getModel().getElementAt(index);

				// Toggle selected state

				item.setSelected(!item.isSelected());

				// Repaint cell

				list.repaint(list.getCellBounds(index, index));
			}
		});

		lstTransaccionalTipoDetalle.setCellRenderer(new CheckBoxListRenderer());

		JButton btnTransaccionalSelecTodoDetalle = new JButton("Seleccionar Todo");
		btnTransaccionalSelecTodoDetalle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizaStatusList(transaccionalTipoDetalle, btnTransaccionalSelecTodoDetalle,
						lstTransaccionalTipoDetalle);
			}
		});
		btnTransaccionalSelecTodoDetalle.setBounds(301, 385, 192, 41);
		pnlTransaccional.add(btnTransaccionalSelecTodoDetalle);

		JPanel pnlTransaccionalRangoFechas = new JPanel();
		pnlTransaccionalRangoFechas.setBorder(
				new TitledBorder(null, "Rango de Fechas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlTransaccionalRangoFechas.setBounds(536, 131, 348, 83);
		pnlTransaccional.add(pnlTransaccionalRangoFechas);
		pnlTransaccionalRangoFechas.setLayout(null);

		JLabel lblTransaccionalFechaInicial = new JLabel("Fecha Inicial");
		lblTransaccionalFechaInicial.setBounds(91, 22, 81, 13);
		pnlTransaccionalRangoFechas.add(lblTransaccionalFechaInicial);

		JLabel lblTransaccionalFechalFinal = new JLabel("Fecha Final ");
		lblTransaccionalFechalFinal.setBounds(95, 52, 81, 13);
		pnlTransaccionalRangoFechas.add(lblTransaccionalFechalFinal);

		dchTransaccionalFInicio = new JDateChooser();
		dchTransaccionalFInicio.setBounds(166, 15, 122, 26);
		dchTransaccionalFInicio.setEnabled(false);
		pnlTransaccionalRangoFechas.add(dchTransaccionalFInicio);

		dchTransaccionalFFinal = new JDateChooser();
		dchTransaccionalFFinal.setBounds(166, 46, 122, 24);
		dchTransaccionalFFinal.setEnabled(false);
		pnlTransaccionalRangoFechas.add(dchTransaccionalFFinal);

		JPanel pnlTransaccionalTipoTransaccion = new JPanel();
		pnlTransaccionalTipoTransaccion.setBorder(
				new TitledBorder(null, "Tipo Transacci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlTransaccionalTipoTransaccion.setBounds(536, 244, 348, 83);
		pnlTransaccional.add(pnlTransaccionalTipoTransaccion);
		pnlTransaccionalTipoTransaccion.setLayout(null);

		JCheckBox cbxTransaccionalNormal = new JCheckBox("Transacción Normal\r\n");
		cbxTransaccionalNormal.setBounds(114, 45, 159, 21);
		pnlTransaccionalTipoTransaccion.add(cbxTransaccionalNormal);

		JCheckBox cbxTransaccionalAbortada = new JCheckBox("Transacción Abortada");
		cbxTransaccionalAbortada.setBounds(114, 22, 159, 21);
		pnlTransaccionalTipoTransaccion.add(cbxTransaccionalAbortada);

		JButton btnTransaccionalGenerarReporte = new JButton("Generar Reporte");
		btnTransaccionalGenerarReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				boolean isValidStartDate =false;
				boolean isValidEndDate =false;
				
				try {

					if(dchTransaccionalFInicio.getDate() == null) {
						JOptionPane.showMessageDialog(null, "La fecha de inicio es incorrecta, sera asignada la fecha del dia de hoy.");
						dchTransaccionalFInicio.setDate(new Date());
						return;
					};
					if(dchTransaccionalFFinal.getDate() == null) {
						JOptionPane.showMessageDialog(null, "La fecha de final es incorrecta, sera asignada la fecha del dia de hoy.");
						dchTransaccionalFFinal.setDate(new Date());
						return;
					};

					
					if (validaTransaccional(items, transaccionalTipoDetalle, dchTransaccionalFInicio.getDate(),
							dchTransaccionalFFinal.getDate(), cbxTransaccionalAbortada.isSelected(),
							cbxTransaccionalNormal.isSelected())) {
						File file = seleccionaArchivo();
						if (file != null) {
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
							btnTransaccionalGenerarReporte.setEnabled(false);
							new Consultas(toSettings).reporteTransaccional(items, transaccionalTipoDetalle,
									dchTransaccionalFInicio.getDate(), dchTransaccionalFFinal.getDate(),
									cbxTransaccionalNormal.isSelected(), cbxTransaccionalAbortada.isSelected(), file);
							btnTransaccionalGenerarReporte.setEnabled(true);
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							JOptionPane.showMessageDialog(null, "Finalizo el reporte transaccional");
						}
					}

				} 
				catch (Exception exGenerar) {
					// TODO: handle exception
					
					JOptionPane.showMessageDialog(null, "El reporte no se pudo generar al existir un error ".concat(exGenerar.getMessage()), "Error al generar el reporte",JOptionPane.ERROR_MESSAGE);
					
					btnTransaccionalGenerarReporte.setEnabled(true);
					tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}

			}
		});
		btnTransaccionalGenerarReporte.setBounds(536, 352, 348, 74);
		pnlTransaccional.add(btnTransaccionalGenerarReporte);

		JPanel pnlPagaTodo = new JPanel();
		pnlPagaTodo.setVisible(false);
		tbpPrincipal.addTab("Paga Todo", null, pnlPagaTodo, null);
		pnlPagaTodo.setLayout(null);

		JLabel lblInstitucionesHeader = new JLabel(
				"A continuación seleccione de entre las opciones disponibles la correspondiente al reporte requerido");
		lblInstitucionesHeader.setBounds(24, 30, 504, 13);
		pnlPagaTodo.add(lblInstitucionesHeader);

		JPanel pnlInstitucionesTipoReporte = new JPanel();
		pnlInstitucionesTipoReporte.setLayout(null);
		pnlInstitucionesTipoReporte.setToolTipText("Seleccione un tipo de reporte");
		pnlInstitucionesTipoReporte.setBorder(new TitledBorder(null, "TIpo de Reporte ", TitledBorder.LEFT,
				TitledBorder.TOP, null, new Color(59, 59, 59)));
		pnlInstitucionesTipoReporte.setBounds(18, 41, 866, 61);
		pnlPagaTodo.add(pnlInstitucionesTipoReporte);

		JRadioButton rbtnInstitucionesActual = new JRadioButton("Reporte del Día Actual\r\n");
		rbtnInstitucionesActual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rbtnInstitucionesActual.isSelected())
					inicializaFechaInstituciones(false);
			}
		});
		buttonGroup_1.add(rbtnInstitucionesActual);
		rbtnInstitucionesActual.setSelected(true);
		rbtnInstitucionesActual.setBounds(175, 19, 178, 21);
		pnlInstitucionesTipoReporte.add(rbtnInstitucionesActual);

		JRadioButton rbtnInstitucionesHistorico = new JRadioButton("Reporte Histórico");
		rbtnInstitucionesHistorico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rbtnInstitucionesHistorico.isSelected())
					inicializaFechaInstituciones(true);
			}
		});

		buttonGroup_1.add(rbtnInstitucionesHistorico);
		rbtnInstitucionesHistorico.setBounds(580, 19, 155, 21);
		pnlInstitucionesTipoReporte.add(rbtnInstitucionesHistorico);


		JScrollPane scrollPagaTodoFarmacias = new JScrollPane();
		scrollPagaTodoFarmacias.setBounds(22, 134, 192, 250);
		pnlPagaTodo.add(scrollPagaTodoFarmacias);

		JList lstPagaTodoFarmacias = new JList(pagaTodoFarmacias);
		lstPagaTodoFarmacias.setToolTipText("Farmacias a ser consultadas");
		scrollPagaTodoFarmacias.setViewportView(lstPagaTodoFarmacias);

		lstPagaTodoFarmacias.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList<CheckboxListItem> list = (JList<CheckboxListItem>) event.getSource();

				// Get index of item clicked
				int index = list.locationToIndex(event.getPoint());
				CheckboxListItem item = (CheckboxListItem) list.getModel().getElementAt(index);

				// Toggle selected state

				item.setSelected(!item.isSelected());

				// Repaint cell

				list.repaint(list.getCellBounds(index, index));
			}
		});

		lstPagaTodoFarmacias.setCellRenderer(new CheckBoxListRenderer());

		JButton btnPagaTodoSelecTodoFarmacias = new JButton("Seleccionar Todo");
		btnPagaTodoSelecTodoFarmacias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizaStatusList(pagaTodoFarmacias, btnPagaTodoSelecTodoFarmacias, lstPagaTodoFarmacias);
			}
		});
		btnPagaTodoSelecTodoFarmacias.setBounds(22, 385, 192, 41);

		pnlPagaTodo.add(btnPagaTodoSelecTodoFarmacias);

		JLabel lblInstitucionesFarmacias = new JLabel("Farmacias Seleccionadas:");
		lblInstitucionesFarmacias.setBounds(24, 114, 156, 13);
		pnlPagaTodo.add(lblInstitucionesFarmacias);

		JLabel lblInstitucionesTipoDetalle = new JLabel("Tipo Detalle:");
		lblInstitucionesTipoDetalle.setBounds(301, 114, 156, 13);
		pnlPagaTodo.add(lblInstitucionesTipoDetalle);

		JScrollPane scrollPagaTodoTipoDetalle = new JScrollPane();
		scrollPagaTodoTipoDetalle.setBounds(301, 134, 192, 250);
		pnlPagaTodo.add(scrollPagaTodoTipoDetalle);

		JList lstPagaTodoTipoDetalle = new JList(pagaTodolTipoDetalle);
		scrollPagaTodoTipoDetalle.setViewportView(lstPagaTodoTipoDetalle);

		lstPagaTodoTipoDetalle.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList<CheckboxListItem> list = (JList<CheckboxListItem>) event.getSource();

				// Get index of item clicked
				int index = list.locationToIndex(event.getPoint());
				CheckboxListItem item = (CheckboxListItem) list.getModel().getElementAt(index);

				// Toggle selected state

				item.setSelected(!item.isSelected());

				// Repaint cell

				list.repaint(list.getCellBounds(index, index));
			}
		});

		lstPagaTodoTipoDetalle.setCellRenderer(new CheckBoxListRenderer());

		JButton btnInstitucionesSelecTodoDetalle = new JButton("Seleccionar Todo");
		btnInstitucionesSelecTodoDetalle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizaStatusList(pagaTodolTipoDetalle, btnInstitucionesSelecTodoDetalle, lstPagaTodoTipoDetalle);
			}
		});
		btnInstitucionesSelecTodoDetalle.setBounds(301, 385, 192, 41);
		pnlPagaTodo.add(btnInstitucionesSelecTodoDetalle);

		JPanel pnlInstitucionesRangoFechas = new JPanel();
		pnlInstitucionesRangoFechas.setBorder(
				new TitledBorder(null, "Rango de Fechas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlInstitucionesRangoFechas.setBounds(536, 131, 348, 83);
		pnlPagaTodo.add(pnlInstitucionesRangoFechas);
		pnlInstitucionesRangoFechas.setLayout(null);

		JLabel lblInstitucionesFechaInicial = new JLabel("Fecha Inicial");
		lblInstitucionesFechaInicial.setBounds(91, 22, 81, 13);
		pnlInstitucionesRangoFechas.add(lblInstitucionesFechaInicial);

		dchpagaTodoFInicio = new JDateChooser();
		dchpagaTodoFInicio.setBounds(166, 15, 122, 26);
		dchpagaTodoFInicio.setEnabled(false);
		pnlInstitucionesRangoFechas.add(dchpagaTodoFInicio);

		JLabel lblInstitucionesFechalFinal = new JLabel("Fecha Final ");
		lblInstitucionesFechalFinal.setBounds(95, 52, 81, 13);
		pnlInstitucionesRangoFechas.add(lblInstitucionesFechalFinal);

		dchpagaTodoFFinal = new JDateChooser();
		dchpagaTodoFFinal.setBounds(166, 46, 122, 24);
		dchpagaTodoFFinal.setEnabled(false);
		pnlInstitucionesRangoFechas.add(dchpagaTodoFFinal);

		JPanel pnlInstitucionesTipoTransaccion = new JPanel();
		pnlInstitucionesTipoTransaccion.setBorder(
				new TitledBorder(null, "Tipo Transacci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlInstitucionesTipoTransaccion.setBounds(536, 244, 348, 83);
		pnlPagaTodo.add(pnlInstitucionesTipoTransaccion);
		pnlInstitucionesTipoTransaccion.setLayout(null);

		JCheckBox cbxpagaTodoNormal = new JCheckBox("Transacción Normal\r\n");
		cbxpagaTodoNormal.setBounds(114, 45, 159, 21);
		pnlInstitucionesTipoTransaccion.add(cbxpagaTodoNormal);

		JCheckBox cbxpagaTodoAbortada = new JCheckBox("Transacción Abortada");
		cbxpagaTodoAbortada.setBounds(114, 22, 159, 21);
		pnlInstitucionesTipoTransaccion.add(cbxpagaTodoAbortada);

		JButton btnPagaTodoGenerarReporte = new JButton("Generar Reporte");
		btnPagaTodoGenerarReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					if (validaTransaccional(pagaTodoFarmacias, pagaTodolTipoDetalle, dchpagaTodoFInicio.getDate(),
							dchpagaTodoFFinal.getDate(), cbxpagaTodoAbortada.isSelected(),
							cbxpagaTodoNormal.isSelected())) {
						File file = seleccionaArchivo();
						if (file != null) {

							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
							btnPagaTodoGenerarReporte.setEnabled(false);
							new Consultas(toSettings).reportePagaTodo(pagaTodoFarmacias, pagaTodolTipoDetalle,
									dchpagaTodoFInicio.getDate(), dchpagaTodoFFinal.getDate(),
									cbxpagaTodoNormal.isSelected(), cbxpagaTodoAbortada.isSelected(), file);
							btnPagaTodoGenerarReporte.setEnabled(true);
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							JOptionPane.showMessageDialog(null, "Finalizo el reporte paga todo ");
						}
					}

				} catch (Exception exGenerar) {
					// TODO: handle exception
					exGenerar.printStackTrace();
				}

			}
		});
		btnPagaTodoGenerarReporte.setBounds(536, 352, 348, 74);
		pnlPagaTodo.add(btnPagaTodoGenerarReporte);

		JPanel pnlReporteCredito = new JPanel();
		pnlReporteCredito.setVisible(false);
		tbpPrincipal.addTab("Reporte de Credito", null, pnlReporteCredito, null);
		pnlReporteCredito.setLayout(null);

		JLabel lblCreditoHeader = new JLabel(
				"A continuación seleccione de entre las opciones disponibles la correspondiente al reporte requerido");
		lblCreditoHeader.setBounds(24, 30, 504, 13);
		pnlReporteCredito.add(lblCreditoHeader);

		JPanel pnlCreditoTipoReporte = new JPanel();
		pnlCreditoTipoReporte.setLayout(null);
		pnlCreditoTipoReporte.setToolTipText("Seleccione un tipo de reporte");
		pnlCreditoTipoReporte.setBorder(new TitledBorder(null, "TIpo de Reporte ", TitledBorder.LEFT, TitledBorder.TOP,
				null, new Color(59, 59, 59)));
		pnlCreditoTipoReporte.setBounds(18, 41, 866, 61);
		pnlReporteCredito.add(pnlCreditoTipoReporte);

		JRadioButton rbtnCreditoActual = new JRadioButton("Reporte del Día Actual\r\n");
		rbtnCreditoActual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rbtnCreditoActual.isSelected())
					inicializaFechaCredito(false);
			}
		});
		buttonGroup_2.add(rbtnCreditoActual);
		rbtnCreditoActual.setSelected(true);
		rbtnCreditoActual.setBounds(175, 19, 178, 21);
		pnlCreditoTipoReporte.add(rbtnCreditoActual);

		JRadioButton rbtnCreditoHistorico = new JRadioButton("Reporte Histórico");
		rbtnCreditoHistorico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rbtnCreditoHistorico.isSelected())
					inicializaFechaCredito(true);
			}
		});
		buttonGroup_2.add(rbtnCreditoHistorico);
		rbtnCreditoHistorico.setBounds(580, 19, 155, 21);
		pnlCreditoTipoReporte.add(rbtnCreditoHistorico);

		JScrollPane scrollCreditoFarmacias = new JScrollPane();
		scrollCreditoFarmacias.setBounds(22, 134, 192, 250);
		pnlReporteCredito.add(scrollCreditoFarmacias);

		JList lstCreditoFarmacias = new JList(reporteCreditoFarmacias);
		lstCreditoFarmacias.setToolTipText("Farmacias a ser consultadas");
		scrollCreditoFarmacias.setViewportView(lstCreditoFarmacias);

		lstCreditoFarmacias.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList<CheckboxListItem> list = (JList<CheckboxListItem>) event.getSource();

				int index = list.locationToIndex(event.getPoint());
				CheckboxListItem item = (CheckboxListItem) list.getModel().getElementAt(index);

				item.setSelected(!item.isSelected());

				list.repaint(list.getCellBounds(index, index));
			}
		});

		lstCreditoFarmacias.setCellRenderer(new CheckBoxListRenderer());

		JButton btnCreditoSelecTodoFarmacias = new JButton("Seleccionar Todo");
		btnCreditoSelecTodoFarmacias.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				actualizaStatusList(reporteCreditoFarmacias, btnCreditoSelecTodoFarmacias, lstCreditoFarmacias);
			}
		});
		btnCreditoSelecTodoFarmacias.setBounds(22, 385, 192, 41);

		pnlReporteCredito.add(btnCreditoSelecTodoFarmacias);

		JLabel lblCreditoFarmacias = new JLabel("Farmacias Seleccionadas:");
		lblCreditoFarmacias.setBounds(24, 114, 156, 13);
		pnlReporteCredito.add(lblCreditoFarmacias);

		JPanel pnlCreditoRangoFechas = new JPanel();
		pnlCreditoRangoFechas.setBorder(
				new TitledBorder(null, "Rango de Fechas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlCreditoRangoFechas.setBounds(536, 131, 348, 83);
		pnlReporteCredito.add(pnlCreditoRangoFechas);
		pnlCreditoRangoFechas.setLayout(null);

		JLabel lblCreditoFechaInicial = new JLabel("Fecha Inicial");
		lblCreditoFechaInicial.setBounds(91, 22, 81, 13);
		pnlCreditoRangoFechas.add(lblCreditoFechaInicial);

		dchCreditoFInicio = new JDateChooser();
		dchCreditoFInicio.setBounds(166, 15, 122, 26);
		dchCreditoFInicio.setEnabled(false);
		pnlCreditoRangoFechas.add(dchCreditoFInicio);

		JLabel lblCreditoFechalFinal = new JLabel("Fecha Final ");
		lblCreditoFechalFinal.setBounds(95, 52, 81, 13);
		pnlCreditoRangoFechas.add(lblCreditoFechalFinal);

		dchCreditoFFinal = new JDateChooser();
		dchCreditoFFinal.setBounds(166, 46, 122, 24);
		dchCreditoFFinal.setEnabled(false);
		pnlCreditoRangoFechas.add(dchCreditoFFinal);

		JPanel pnlCreditoTipoTransaccion = new JPanel();
		pnlCreditoTipoTransaccion.setBorder(
				new TitledBorder(null, "Tipo Transacci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlCreditoTipoTransaccion.setBounds(536, 244, 348, 83);
		pnlReporteCredito.add(pnlCreditoTipoTransaccion);
		pnlCreditoTipoTransaccion.setLayout(null);

		JCheckBox cbxCreditoNormal = new JCheckBox("Transacción Normal\r\n");
		cbxCreditoNormal.setBounds(114, 45, 159, 21);
		pnlCreditoTipoTransaccion.add(cbxCreditoNormal);

		JCheckBox cbxCreditoAbortada = new JCheckBox("Transacción Abortada");
		cbxCreditoAbortada.setBounds(114, 22, 159, 21);
		pnlCreditoTipoTransaccion.add(cbxCreditoAbortada);

		JButton btnCreditoGenerarReporte = new JButton("Generar Reporte");
		btnCreditoGenerarReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					if (validaTransaccional(reporteCreditoFarmacias, reporteCreditoFarmacias,
							dchCreditoFInicio.getDate(), dchCreditoFFinal.getDate(), cbxCreditoAbortada.isSelected(),
							cbxCreditoNormal.isSelected())) {
						File file = seleccionaArchivo();
						if (file != null) {

							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
							btnCreditoGenerarReporte.setEnabled(false);
							new Consultas(toSettings).reporteCredito(reporteCreditoFarmacias, reporteCreditoFarmacias,
									dchCreditoFInicio.getDate(), dchCreditoFFinal.getDate(),
									cbxCreditoNormal.isSelected(), cbxCreditoAbortada.isSelected(), file);
							btnCreditoGenerarReporte.setEnabled(true);
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							JOptionPane.showMessageDialog(null, "Finalizo el reporte credito ");
						}
					}

				} catch (Exception exGenerar) {
					// TODO: handle exception
					exGenerar.printStackTrace();
				}

			}
		});
		btnCreditoGenerarReporte.setBounds(536, 352, 348, 74);
		pnlReporteCredito.add(btnCreditoGenerarReporte);

		JPanel pnlPreciosInstitucion = new JPanel();
		pnlPreciosInstitucion.setVisible(false);
		tbpPrincipal.addTab("Reporte Precios Institución", null, pnlPreciosInstitucion, null);
		pnlPreciosInstitucion.setLayout(null);

		JLabel lblNewLabel = new JLabel("Seleccione una Farmacia");
		lblNewLabel.setBounds(245, 28, 181, 13);
		pnlPreciosInstitucion.add(lblNewLabel);

		JComboBox cbxInstitucionFarmacia = new JComboBox(institucionesFarmaciasCbx);
		cbxInstitucionFarmacia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		cbxInstitucionFarmacia.setToolTipText("Farmacia a ser consultadar");
		cbxInstitucionFarmacia.setBounds(245, 45, 299, 41);
		pnlPreciosInstitucion.add(cbxInstitucionFarmacia);

		JLabel lblNewLabel_1 = new JLabel("Farmacias Seleccionadas");
		lblNewLabel_1.setBounds(245, 110, 226, 13);
		pnlPreciosInstitucion.add(lblNewLabel_1);

		scrollInstitucionesFarmacias = new JScrollPane();
		scrollInstitucionesFarmacias.setBounds(245, 135, 299, 171);
		pnlPreciosInstitucion.add(scrollInstitucionesFarmacias);

		lstInstitucionesFarmacias = new JList(institucionesFarmacias);
		lstInstitucionesFarmacias.setToolTipText("Farmacias a ser consultadas");
		scrollInstitucionesFarmacias.setViewportView(lstInstitucionesFarmacias);

		lstInstitucionesFarmacias.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent event) {
				JList<CheckboxListItem> list = (JList<CheckboxListItem>) event.getSource();

				int index = list.locationToIndex(event.getPoint());
				CheckboxListItem item = (CheckboxListItem) list.getModel().getElementAt(index);

				item.setSelected(!item.isSelected());

				list.repaint(list.getCellBounds(index, index));
			}
		});

		lstInstitucionesFarmacias.setCellRenderer(new CheckBoxListRenderer());

		JButton btnInstitucionGeneraReporte = new JButton("Generar Reporte");
		btnInstitucionGeneraReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (validaPreciosInstitucion(cbxInstitucionFarmacia, institucionesFarmacias)) {
						lstInstitucionesFarmacias.updateUI();
						File file = seleccionaArchivo();
						if (file != null) {
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
							btnInstitucionGeneraReporte.setEnabled(false);
							new Consultas(toSettings).reporteInstitucion(institucionesFarmacias, cbxInstitucionFarmacia,
									file);
							btnInstitucionGeneraReporte.setEnabled(true);
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							JOptionPane.showMessageDialog(null, "Finalizo el reporte precios insitucion ");
						}
					}

				} catch (Exception exGenerar) {
					// TODO: handle exception
					exGenerar.printStackTrace();
				}

			}

		});
		btnInstitucionGeneraReporte.setBounds(245, 320, 299, 74);
		pnlPreciosInstitucion.add(btnInstitucionGeneraReporte);

		btnInstitucionSelecciona = new JButton("Seleccionar Todo");
		btnInstitucionSelecciona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cbxInstitucionFarmacia.setSelectedIndex(0);
				actualizaStatusList(institucionesFarmacias, btnInstitucionSelecciona, lstInstitucionesFarmacias);
			}

		});
		btnInstitucionSelecciona.setBounds(568, 135, 192, 71);
		pnlPreciosInstitucion.add(btnInstitucionSelecciona);

		JPanel pnlDevoluciones = new JPanel();
		pnlDevoluciones.setVisible(false);
		tbpPrincipal.addTab("Devoluciones vs Reimpresión de Venta", null, pnlDevoluciones, null);
		pnlDevoluciones.setLayout(null);

		JLabel lblNewLabel_2 = new JLabel("Farmacias Seleccionadas");
		lblNewLabel_2.setBounds(261, 50, 207, 13);
		pnlDevoluciones.add(lblNewLabel_2);

		JComboBox cbxDevolucionFarmacia = new JComboBox(devolucionFarmaciasCbx);
		cbxDevolucionFarmacia.setToolTipText("Farmacia a ser consultadar");
		cbxDevolucionFarmacia.setBounds(261, 73, 342, 41);
		pnlDevoluciones.add(cbxDevolucionFarmacia);

		JPanel pnlDevRangoFechas = new JPanel();
		pnlDevRangoFechas
				.setBorder(new TitledBorder(null, "Rango Fechas", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlDevRangoFechas.setBounds(260, 153, 349, 103);
		pnlDevoluciones.add(pnlDevRangoFechas);
		pnlDevRangoFechas.setLayout(null);

		JLabel lblNewLabel_3 = new JLabel("Fecha Inicial ");
		lblNewLabel_3.setBounds(62, 29, 83, 13);
		pnlDevRangoFechas.add(lblNewLabel_3);

		dchDevFInicial = new JDateChooser();
		dchDevFInicial.setBounds(145, 23, 122, 26);
		pnlDevRangoFechas.add(dchDevFInicial);

		JLabel lblNewLabel_4 = new JLabel("Fecha Final ");
		lblNewLabel_4.setBounds(68, 72, 65, 13);
		pnlDevRangoFechas.add(lblNewLabel_4);

		dchDevFFinal = new JDateChooser();
		dchDevFFinal.setBounds(145, 64, 122, 26);
		pnlDevRangoFechas.add(dchDevFFinal);

		JButton btnDevGeneraReporte = new JButton("Generar Reporte");
		btnDevGeneraReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (validaDevoluciones(cbxDevolucionFarmacia, dchDevFInicial.getDate(), dchDevFFinal.getDate())) {
						File file = seleccionaArchivo();
						if (file != null) {
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
							btnDevGeneraReporte.setEnabled(false);
							new Consultas(toSettings).reporteDevolucion(cbxDevolucionFarmacia, dchDevFInicial.getDate(),
									dchDevFFinal.getDate(), file);
							btnDevGeneraReporte.setEnabled(true);
							tbpPrincipal.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
							JOptionPane.showMessageDialog(null, "Finalizo el reporte precios insitucion ");
						}

					} 
				} catch (Exception e2) {
					// TODO: handle exception
					e2.printStackTrace();
				}

			}
		});
		btnDevGeneraReporte.setBounds(261, 285, 348, 74);
		pnlDevoluciones.add(btnDevGeneraReporte);

		inicializaFechaTransaccional(false);
		inicializaFechaInstituciones(false);
		inicializaFechaCredito(false);
		inicializaFechaDev(false);
		JOptionPane.showMessageDialog(null, "Esta aplicación es una solución temporal");

	}

	private void inicializaFechaTransaccional(Boolean isEnabled) {
		dchTransaccionalFInicio.setDate(new Date());
		dchTransaccionalFFinal.setDate(new Date());
		dchTransaccionalFInicio.setEnabled(isEnabled);
		dchTransaccionalFFinal.setEnabled(isEnabled);

	}

	private void inicializaFechaInstituciones(Boolean isEnabled) {
		dchpagaTodoFInicio.setDate(new Date());
		dchpagaTodoFFinal.setDate(new Date());
		dchpagaTodoFInicio.setEnabled(isEnabled);
		dchpagaTodoFFinal.setEnabled(isEnabled);

	}

	private void inicializaFechaCredito(Boolean isEnabled) {
		dchCreditoFInicio.setDate(new Date());
		dchCreditoFFinal.setDate(new Date());
		dchCreditoFInicio.setEnabled(isEnabled);
		dchCreditoFFinal.setEnabled(isEnabled);

	}

	private void inicializaFechaDev(Boolean isEnabled) {
		dchDevFInicial.setDate(new Date());
		dchDevFFinal.setDate(new Date());

	}

	private void actualizaStatusList(CheckboxListItem[] listItem, Object boton, Object lista) {

		if (((JButton) boton).getText().equals("Seleccionar Todo")) {
			for (CheckboxListItem c : listItem) {
				c.setSelected(true);
			}
			((JList<CheckboxListItem>) lista).updateUI();
			((JButton) boton).setText("No Seleccionar Todo");
		} else {
			for (CheckboxListItem c : listItem) {
				c.setSelected(false);
			}
			((JList<CheckboxListItem>) lista).updateUI();
			((JButton) boton).setText("Seleccionar Todo");
		}

	}

	private CheckboxListItem[] leeCatalogFarmacia(ToSetting toSetting, Boolean esCombo) {

		List<CheckboxListItem> farmacias = new ArrayList<>();
		toSetting.getFarmacias().forEach(x -> {
			farmacias.add(new CheckboxListItem(x.getId(), x.getDescripcion(), x));
		});
		if (esCombo)
			farmacias.add(0, new CheckboxListItem(-1, "--- Seleccione ---"));

		return farmacias.toArray(new CheckboxListItem[farmacias.size()]);
	}

	private CheckboxListItem[] leeCatalogTipoDetalle(ToSetting toSetting) {
		List<CheckboxListItem> tipoDetalles = new ArrayList<>();
		toSetting.getTipoDetalle().forEach(x -> {
			tipoDetalles.add(new CheckboxListItem(x.getId(), x.getDescripcion(), x));
		});
		return tipoDetalles.toArray(new CheckboxListItem[tipoDetalles.size()]);
	}

	private File seleccionaArchivo() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showOpenDialog(null);
		File file = fileChooser.getSelectedFile();
		if (file != null) {
			int opcion = JOptionPane.showConfirmDialog(null,
					"¿Esta seguro de generar el reporte en  ".concat(file.getAbsolutePath()).concat("?"));
			if (opcion != 0)
				file = null;

		}
		return file;

	}

	private Boolean validaTransaccional(CheckboxListItem[] farmacias, CheckboxListItem[] tipoDetalle, Date fechaInicial,
			Date fechaFinal, Boolean abortada, Boolean normal) {

		if (Arrays.asList(farmacias).stream().filter(x -> x.isSelected() == true).findAny().orElse(null) == null) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una farmacia");
			return false;
		}

		if (Arrays.asList(tipoDetalle).stream().filter(x -> x.isSelected() == true).findAny().orElse(null) == null) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar un tipo detalle");
			return false;
		}

		if (fechaInicial.after(fechaFinal)) {
			JOptionPane.showMessageDialog(null, "La fecha inicial debe ser menor a la fecha final");
			return false;
		}

		if (!abortada && !normal) {
			JOptionPane.showMessageDialog(null, "Debe de seleccionar un tipo de transacción");
			return false;
		}

		return true;

	}

	private Boolean validaPreciosInstitucion(Object combo, CheckboxListItem[] farmacias) {

		if (((CheckboxListItem) (((JComboBox) combo).getSelectedItem())).getId() == 0 && Arrays.asList(farmacias)
				.stream().filter(x -> x.isSelected() == true).findAny().orElse(null) == null) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una farmacia o varias farmacias de la lista");
			return false;

		}

		if (((CheckboxListItem) (((JComboBox) combo).getSelectedItem())).getId() > 0 && Arrays.asList(farmacias)
				.stream().filter(x -> x.isSelected() == true).findAny().orElse(null) != null) {

			int respuesta = JOptionPane.showConfirmDialog(null,
					"Usted ha seleccionado varios opciones de farmacia, individual y conjunto de farmarcias. ¿La busqueda se realizara por el conjunto de farmacias seleccionadas? ");

			switch (respuesta) {
			case 0:
				((JComboBox) combo).setSelectedIndex(0);
				break;
			case 1:
				Arrays.asList(farmacias).forEach(x -> {
					x.setSelected(false);
				});
				break;
			case 2:
				return false;
			}
		}
		return true;
	}

	private Boolean validaDevoluciones(Object combo, Date fechaInicio, Date fechaFin) {

		if (((CheckboxListItem) (((JComboBox) combo).getSelectedItem())).getId() == -1) {
			JOptionPane.showMessageDialog(null, "Debe seleccionar una farmacia ");
			return false;

		}

		if (fechaInicio.after(fechaFin)) {
			JOptionPane.showMessageDialog(null, "La fecha incio debe ser mayor o igual a la fecha final ");
			return false;
		}

		return true;
	}

	private void genera() {

		ToSetting toSetting = new ToSetting();

		try {

			toSetting.setGeneral(new General());
			toSetting.setRemota(new Remota());
			toSetting.setFarmacias(new ArrayList<>());
			toSetting.setTipoDetalle(new ArrayList<>());


			toSetting.getGeneral().setLlavePrivada("privada.dat");
			toSetting.getGeneral().setArchivoKey("Key.dat");
			toSetting.getGeneral().setUrl("localhost");
			toSetting.getGeneral().setPuerto("1433");
			toSetting.getGeneral().setBaseDatos("TPATxHistory");
			toSetting.getGeneral().setNumeroTransacciones(1000000L);


			toSetting.getRemota().setLlavePrivada("privada.dat");
			toSetting.getRemota().setArchivoKey("Key.dat");
			toSetting.getRemota().setUrl("localhost");
			toSetting.getRemota().setPuerto("1433");
			toSetting.getRemota().setBaseDatos("TPCentralDB");

			for (int i = 1; i <= 138; i++) {
				toSetting.getFarmacias().add(new Farmacia(i, "Farmacia ".concat(Integer.toString(i)), "192.168.1.66"));
			}

			for (int i = 1; i <= 55; i++) {
				toSetting.getTipoDetalle().add(new TipoDetalle(i, "Tipo Detalle ".concat(Integer.toString(i))));
			}

			ObjectMapper mapper = new ObjectMapper();

			mapper.writeValue(new File("transaccional.json"), toSetting);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private ToSetting leeSeettings() {

		ObjectMapper mapper = new ObjectMapper();
		ToSetting toSettings = null;

		try {
			toSettings = mapper.readValue(new File("transaccional.json"), ToSetting.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Es neceesario contar con el archivo transactional.json, para el funcionamiento de la aplicación.");
			
		}

		return toSettings;
	}


}
