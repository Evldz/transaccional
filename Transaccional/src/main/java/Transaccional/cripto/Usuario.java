package Transaccional.cripto;

/**
* @Author  Ramses 
*/
import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Usuario implements Serializable {


	private static final long serialVersionUID = 8960038641030847893L;
	private String usuario;
	private String password;
}
