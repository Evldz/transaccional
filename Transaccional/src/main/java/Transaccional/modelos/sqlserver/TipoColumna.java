package Transaccional.modelos.sqlserver;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TipoColumna {
	private String columna;
	private int posicion;
	private int tipo;
	
	public TipoColumna(String columna, int posicion, int tipo) {
		
		this.columna = columna;
		this.posicion = posicion;
		this.tipo = tipo;
	}

}
