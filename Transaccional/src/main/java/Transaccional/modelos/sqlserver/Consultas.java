package Transaccional.modelos.sqlserver;

/**
 * @author Ramses Arellano Rincon
 * 
 */
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import Transaccional.cripto.Usuario;
import Transaccional.modelos.list.checkbox.CheckboxListItem;
import Transaccional.modelos.settings.Farmacia;
import Transaccional.modelos.settings.ToSetting;

public class Consultas {
	private static Cipher rsa;
	private ToSetting toSetting;
	private String connectionUrlLocal;
	private String connectionUrlRemota;

	public Consultas(ToSetting toSetting) throws Exception {
		 
		Usuario usuario = new ObjectMapper().readValue(Desencripta(new File(toSetting.getGeneral().getLlavePrivada()), new File(toSetting.getGeneral().getArchivoKey())), Usuario.class);
		if (usuario == null) {throw new Exception();}
		this.toSetting = toSetting;
		this.connectionUrlLocal = "jdbc:sqlserver://".concat(toSetting.getGeneral().getUrl()).concat(":")
				.concat(toSetting.getGeneral().getPuerto()).concat(";databaseName=")
				.concat(toSetting.getGeneral().getBaseDatos()).concat(";user=")
				.concat(usuario.getUsuario()).concat(";password=")
				.concat(usuario.getPassword());
		try {
			DriverManager.registerDriver(new com.microsoft.sqlserver.jdbc.SQLServerDriver());
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}


	public List<TipoColumna> obtenTupla(ResultSet rs) {

		List<TipoColumna> columnas = new ArrayList<TipoColumna>();
		if (rs == null)
			return columnas;

		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				columnas.add(new TipoColumna(rsmd.getColumnName(i), i, rsmd.getColumnType(i)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return columnas;
	}

	public String generaRegistro(List<TipoColumna> columnas, ResultSet rs) {

		String registro = "";
		if (rs == null)
			return "";

		try {
			for (TipoColumna t : columnas) {

				if (rs.getString(t.getColumna()) == null) {
					registro = registro.concat("NULL");
				} else {

					switch (t.getTipo()) {
					case java.sql.Types.BIGINT:
						registro = registro.concat(rs.getBigDecimal(t.getColumna()).toString());
						break;
					case java.sql.Types.BINARY:
						registro = registro.concat(rs.getBinaryStream(t.getColumna()).toString());
						break;
					case java.sql.Types.BIT:
						registro = registro.concat(new String(rs.getBytes(t.getColumna())));
						break;
					case java.sql.Types.CHAR:
					case java.sql.Types.NVARCHAR:
					case java.sql.Types.LONGNVARCHAR:
					case java.sql.Types.VARCHAR:
						registro = registro.concat(rs.getString(t.getColumna()));
						break;
					case java.sql.Types.INTEGER:
						registro = registro.concat(Integer.toString(rs.getInt(t.getColumna())));
						break;
					case java.sql.Types.NUMERIC:
					case java.sql.Types.DECIMAL:
					case java.sql.Types.DOUBLE:
						registro = registro.concat(Double.toString(rs.getDouble(t.getColumna())));
						break;
					case java.sql.Types.DATE:
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
						registro = registro.concat(simpleDateFormat.format(rs.getDate(t.getColumna())));
						break;
					case java.sql.Types.TIME:
						registro = registro.concat(rs.getTime(t.getColumna()).toString());
						break;
					default:
						registro = registro.concat("No puedo obtener el campo").concat(t.getColumna());
						break;

					}
				}
				registro = registro.concat(",");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return registro.length() == 0 ? "" : registro.substring(0, registro.length() - 1);
	}

	public void reporteTransaccional(CheckboxListItem[] farmacias, CheckboxListItem[] tipoDetalle, Date fechaInicio,
			Date fechaFinal, Boolean registroNormal, Boolean registroAbortado, File file) {

		String codicionFarmacia = generaIn(farmacias, "[No. Farmacia]");
		String codicionTipoDetalle = generaIn(tipoDetalle, "[Tipo Detalle]");
		String condicionFecha = generaDate(fechaInicio, fechaFinal, "[Fecha]");
		String condicionTransaccion = generaTransaccion(registroNormal, registroAbortado, "[Ticket Abortado]");

		List<Catalogo> datos = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(connectionUrlLocal);
				Statement stmt = con.createStatement();) {
			String SQL = "SELECT * FROM VwBaseReport ";
			String condicion = "";

			condicion = " WHERE ".concat(condicionFecha);
			condicion = condicionTransaccion.length() == 0 ? condicion
					: condicion.concat(" AND ").concat(condicionTransaccion);
			condicion = codicionFarmacia.length() == 0 ? condicion : condicion.concat(" AND ").concat(codicionFarmacia);
			condicion = codicionTipoDetalle.length() == 0 ? condicion
					: condicion.concat(" AND ").concat(codicionTipoDetalle);
			SQL = SQL.concat(condicion);
			SQL = SQL.concat(" Order By [No. Farmacia] ASC");
			System.out.println(SQL);
			ResultSet rs = stmt.executeQuery(SQL);
			List<TipoColumna> tupla = obtenTupla(rs);

			String aux = "";
			// Iterate through the data in the result set and display it.
			List<String> filas = new ArrayList<>();
			Long inicioMilisegundos = System.currentTimeMillis();
			while (rs.next()) {

				filas.add(generaRegistro(tupla, rs));
				if (filas.size() > toSetting.getGeneral().getNumeroTransacciones()) {
					FileUtils.writeLines(file, filas, true);
					filas.clear();
				}

			}
			if (filas.size() > 0) {
				FileUtils.writeLines(file, filas, true);
				filas.clear();
			}
			System.out.println("Tardo " + (System.currentTimeMillis() - inicioMilisegundos));
		}
		// Handle any errors that may have occurred.
		catch (Exception e1) {
			e1.printStackTrace();
		}

		System.out.println(codicionFarmacia);
		System.out.println(codicionTipoDetalle);
		System.out.println(condicionFecha);
		System.out.println(condicionTransaccion);

	}

	public void reportePagaTodo(CheckboxListItem[] farmacias, CheckboxListItem[] tipoDetalle, Date fechaInicio,
			Date fechaFinal, Boolean registroNormal, Boolean registroAbortado, File file) {

		String codicionFarmacia = generaIn(farmacias, "[No. Farmacia]");
		String codicionTipoDetalle = generaIn(tipoDetalle, "[Tipo Detalle]");
		String condicionFecha = generaDate(fechaInicio, fechaFinal, "[Fecha]");
		String condicionTransaccion = generaTransaccion(registroNormal, registroAbortado, "[Ticket Abortado]");

		List<Catalogo> datos = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(connectionUrlLocal);
				Statement stmt = con.createStatement();) {
			String SQL = "SELECT [No. Farmacia], Item, Descripción, [Precio Final al Cliente], [No. Conf. PagaTodo], [No. Trans. PagaTodo], Caja, Num_Venta, [Núm. Empleado], [Nombre Vendedor], Unidades, [Precio Venta Sin IVA], IVA "
					.concat(" FROM VwBaseReport ");
			String condicion = "";

			condicion = " WHERE ".concat(condicionFecha);
			condicion = condicionTransaccion.length() == 0 ? condicion
					: condicion.concat(" AND ").concat(condicionTransaccion);
			condicion = codicionFarmacia.length() == 0 ? condicion : condicion.concat(" AND ").concat(codicionFarmacia);
			condicion = codicionTipoDetalle.length() == 0 ? condicion
					: condicion.concat(" AND ").concat(codicionTipoDetalle);
			SQL = SQL.concat(condicion);
			SQL = SQL.concat(" Order By [No. Farmacia] ASC");
			System.out.println(SQL);
			ResultSet rs = stmt.executeQuery(SQL);
			List<TipoColumna> tupla = obtenTupla(rs);

			String aux = "";
			// Iterate through the data in the result set and display it.
			List<String> filas = new ArrayList<>();
			Long inicioMilisegundos = System.currentTimeMillis();
			while (rs.next()) {

				filas.add(generaRegistro(tupla, rs));
				if (filas.size() > toSetting.getGeneral().getNumeroTransacciones()) {
					FileUtils.writeLines(file, filas, true);
					filas.clear();
				}

			}
			if (filas.size() > 0) {
				FileUtils.writeLines(file, filas, true);
				filas.clear();
			}
			System.out.println("Tardo " + (System.currentTimeMillis() - inicioMilisegundos));
		}
		// Handle any errors that may have occurred.
		catch (Exception e1) {
			e1.printStackTrace();
		}

		System.out.println(codicionFarmacia);
		System.out.println(codicionTipoDetalle);
		System.out.println(condicionFecha);
		System.out.println(condicionTransaccion);

	}

	public void reporteCredito(CheckboxListItem[] farmacias, CheckboxListItem[] tipoDetalle, Date fechaInicio,
			Date fechaFinal, Boolean registroNormal, Boolean registroAbortado, File file) {

		String codicionFarmacia = generaIn(farmacias, "[No. Farmacia]");
		String condicionFecha = generaDate(fechaInicio, fechaFinal, "[Fecha]");
		String condicionTransaccion = generaTransaccion(registroNormal, registroAbortado, "[Ticket Abortado]");

		List<Catalogo> datos = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(connectionUrlLocal);
				Statement stmt = con.createStatement();) {
			String SQL = "SELECT * ".concat(" FROM VwBaseReport ");
			String condicion = "";

			condicion = " WHERE ".concat(condicionFecha);
			condicion = condicionTransaccion.length() == 0 ? condicion
					: condicion.concat(" AND ").concat(condicionTransaccion);
			condicion = codicionFarmacia.length() == 0 ? condicion : condicion.concat(" AND ").concat(codicionFarmacia);

			SQL = SQL.concat(condicion);
			SQL = SQL.concat(" Order By [No. Farmacia] ASC");
			System.out.println(SQL);
			ResultSet rs = stmt.executeQuery(SQL);
			List<TipoColumna> tupla = obtenTupla(rs);

			String aux = "";
			// Iterate through the data in the result set and display it.
			List<String> filas = new ArrayList<>();
			Long inicioMilisegundos = System.currentTimeMillis();
			while (rs.next()) {

				filas.add(generaRegistro(tupla, rs));
				if (filas.size() > toSetting.getGeneral().getNumeroTransacciones()) {
					FileUtils.writeLines(file, filas, true);
					filas.clear();
				}

			}
			if (filas.size() > 0) {
				FileUtils.writeLines(file, filas, true);
				filas.clear();
			}
			System.out.println("Tardo " + (System.currentTimeMillis() - inicioMilisegundos));
		}
		// Handle any errors that may have occurred.
		catch (Exception e1) {
			e1.printStackTrace();
		}

		System.out.println(codicionFarmacia);
		System.out.println(condicionFecha);
		System.out.println(condicionTransaccion);

	}

	public void reporteInstitucion(CheckboxListItem[] farmacias, JComboBox combo, File file) throws Exception {

		List<Catalogo> datos = new ArrayList<>();
		List<Farmacia> busquedas = new ArrayList<>();

		Arrays.asList(farmacias).forEach(x -> {
			if (x.isSelected()) {
				busquedas.add((Farmacia) x.getObject());
			}
		});
		if (combo.getSelectedIndex() > 0) {
			busquedas.add(((Farmacia) ((CheckboxListItem) combo.getSelectedItem()).getObject()));
		}

		String coneccion = "";
		String SQL = "";

		
		Usuario usuario = new ObjectMapper().readValue(Desencripta(new File(toSetting.getRemota().getLlavePrivada()), new File(toSetting.getRemota().getArchivoKey())), Usuario.class);
		for (Farmacia farmacia : busquedas) {

			System.out.println("Farmacia ".concat(Integer.toString(farmacia.getId())));
			SQL = "SELECT * FROM GSPInstitutionPrices WHERE lRetailStoreID='1' ";

			coneccion = "jdbc:sqlserver://".concat(farmacia.getUrl()).concat(":")
					.concat(toSetting.getRemota().getPuerto()).concat(";databaseName=")
					.concat(toSetting.getRemota().getBaseDatos()).concat(";user=")
					.concat(usuario.getUsuario()).concat(";password=")
					.concat(usuario.getPassword());

			try (Connection con = DriverManager.getConnection(coneccion); Statement stmt = con.createStatement();) {

				ResultSet rs = stmt.executeQuery(SQL);
				List<TipoColumna> tupla = obtenTupla(rs);

				// Iterate through the data in the result set and display it.
				List<String> filas = new ArrayList<>();
				Long inicioMilisegundos = System.currentTimeMillis();
				while (rs.next()) {
					filas.add(generaRegistro(tupla, rs));
					if (filas.size() > toSetting.getGeneral().getNumeroTransacciones()) {
						FileUtils.writeLines(file, filas, true);
						filas.clear();
					}
				}
				if (filas.size() > 0) {
					FileUtils.writeLines(file, filas, true);
					filas.clear();
				}
				System.out.println("Tardo " + (System.currentTimeMillis() - inicioMilisegundos));
			}
			// Handle any errors that may have occurred.
			catch (Exception e1) {
				e1.printStackTrace();
			}

		}

	}

	public void reporteDevolucion(JComboBox combo, Date fechaInicio, Date fechaFinal, File file) {

		String codicionFarmacia = "";
		
		if(combo.getSelectedIndex() > 0) {
			codicionFarmacia = " AND [No. Farmacia]".concat(" = ")
			.concat(Integer.toString(((CheckboxListItem) combo.getSelectedItem()).getId()));

		}
		
		String condicionFecha = " AND ".concat(generaDate(fechaInicio, fechaFinal, "[Fecha]"));

		List<Catalogo> datos = new ArrayList<>();

		try (Connection con = DriverManager.getConnection(connectionUrlLocal); Statement stmt = con.createStatement();) {
			
			String SQL = "SELECT * FROM VwBaseReport WHERE  [Canal Venta]='Presentation.DimGSPSaleType.ReturnMostrador' ".concat(codicionFarmacia).concat(condicionFecha);

			System.out.println(SQL);
			ResultSet rs = stmt.executeQuery(SQL);
			List<TipoColumna> tupla = obtenTupla(rs);

			String aux = "";
			// Iterate through the data in the result set and display it.
			List<String> filas = new ArrayList<>();
			Long inicioMilisegundos = System.currentTimeMillis();
			while (rs.next()) {

				filas.add(generaRegistro(tupla, rs));
				if (filas.size() > toSetting.getGeneral().getNumeroTransacciones()) {
					FileUtils.writeLines(file, filas, true);
					filas.clear();
				}

			}
			if (filas.size() > 0) {
				FileUtils.writeLines(file, filas, true);
				filas.clear();
			}
			System.out.println("Tardo " + (System.currentTimeMillis() - inicioMilisegundos));
		}
		// Handle any errors that may have occurred.
		catch (Exception e1) {
			e1.printStackTrace();
		}

		System.out.println(codicionFarmacia);
		System.out.println(condicionFecha);

	}

	public String generaIn(CheckboxListItem[] in, String campo) {

		String condicion = "";
		if (Arrays.asList(in).stream().filter(x -> x.isSelected()).count() == in.length || in == null
				|| in.length == 0) {
			return condicion;
		}

//		condicion = condicion.concat(campo).concat(" in (");
		for (CheckboxListItem c : Arrays.asList(in)) {
			if (c.isSelected()) {
				condicion = condicion.concat(Integer.toString(c.getId())).concat(",");
			}
		}
		if (condicion.length() > 0)
			condicion = campo.concat(" in ( ").concat(condicion.substring(0, condicion.length() - 1)).concat(")");
		return condicion;
	}

	public String generaDate(Date inicio, Date fin, String campo) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
		Date inicio2 = null;
		Date fin2 = null;

		try {
			inicio2 = simpleDateFormat.parse(simpleDateFormat.format(inicio));
			fin2 = simpleDateFormat.parse(simpleDateFormat.format(fin));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return inicio2.equals(fin2) ? campo.concat(" = '").concat(simpleDateFormat.format(inicio)).concat("'")
				: campo.concat(" BETWEEN  '").concat(simpleDateFormat.format(inicio)).concat("'").concat(" AND '")
						.concat(simpleDateFormat.format(fin)).concat("'");
	}

	public String generaTransaccion(Boolean normal, Boolean abortada, String campo) {

		String condicion = "";
		if (normal && abortada) {
			return condicion;
		}
		if (normal)
			condicion = condicion.concat(campo).concat(" !=1");
		if (abortada)
			condicion = condicion.concat(campo).concat(" = 1");
		return condicion;
	}
	
	private static String  Desencripta(File llavePrivada, File llaveKey) throws Exception {

		String textoDesencripado=null;
		try {
			PrivateKey privateKey = loadPrivateKey(llavePrivada.getAbsolutePath());
			byte[] encriptado = loadPass(llaveKey.getAbsolutePath());
			if (privateKey == null) {
				JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta al no leer la llave privada");
			}
			if (encriptado.length == 0) {
				JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta al no leer el archivo key");
			}
			rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			rsa.init(Cipher.DECRYPT_MODE, privateKey);
			byte[] bytesDesencriptados = rsa.doFinal(encriptado);
			textoDesencripado = new String(bytesDesencriptados);
		} catch (Exception e) {

			JOptionPane.showMessageDialog(null, "No se pudo realizar la consulta al no poder leer las llaves. ".concat(e.getMessage()), "Error al desencriptar",JOptionPane.ERROR_MESSAGE);
		}

		return textoDesencripado;
	}

	private static PrivateKey loadPrivateKey(String fileName) throws Exception {

		FileInputStream fis = new FileInputStream(fileName);
		int numBtyes = fis.available();
		byte[] bytes = new byte[numBtyes];
		fis.read(bytes);
		fis.close();

		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		KeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
		PrivateKey keyFromBytes = keyFactory.generatePrivate(keySpec);
		return keyFromBytes;
	}

	private static byte[] loadPass(String fileName) throws Exception {
		FileInputStream fis = new FileInputStream(fileName);
		int numBtyes = fis.available();
		byte[] bytes = new byte[numBtyes];
		fis.read(bytes);
		fis.close();
		return bytes;
	}
	

}
