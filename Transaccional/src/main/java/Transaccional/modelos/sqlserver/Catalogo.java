package Transaccional.modelos.sqlserver;

import lombok.Data;


@Data
public class Catalogo {
	
	private int id;
	private String descripcion;
	
	public Catalogo(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	
	

}
