package Transaccional.modelos.settings;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@Data
public class ToSetting implements Serializable {
	

	private static final long serialVersionUID = 4732518197480162588L;
	private General general;
	private Remota remota;
	private List<Farmacia> farmacias;
	private List<TipoDetalle> tipoDetalle;
	
}
