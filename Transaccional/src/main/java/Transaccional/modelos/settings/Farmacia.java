package Transaccional.modelos.settings;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Farmacia implements Serializable {
	

	private static final long serialVersionUID = 5726870568557847378L;
	private int id;
	private String descripcion;
	private String url;
	
	public Farmacia(int id, String descripcion, String url) {
		this.id = id;
		this.descripcion = descripcion;
		this.url = url;
	}
	
	
	

}
