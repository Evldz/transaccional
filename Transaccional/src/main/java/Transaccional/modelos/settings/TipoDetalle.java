package Transaccional.modelos.settings;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class TipoDetalle implements Serializable{
	

	private static final long serialVersionUID = 6099095839016750646L;
	private int id;
	private String descripcion;
	
	public TipoDetalle(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}
	
	
}
