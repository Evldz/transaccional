package Transaccional.modelos.settings;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Remota implements Serializable {
	

	private static final long serialVersionUID = -7291289700813285185L;
	private String llavePrivada;
	private String archivoKey;
	private String url;
	private String puerto;
	private String baseDatos;

}
