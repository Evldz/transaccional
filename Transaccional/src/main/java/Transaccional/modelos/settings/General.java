package Transaccional.modelos.settings;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class General implements Serializable {
	

	private static final long serialVersionUID = -5573314623091035386L;
	
	private String llavePrivada;
	private String archivoKey;
	private String url;
	private String puerto;
	private String baseDatos;
	private Long numeroTransacciones;

}
