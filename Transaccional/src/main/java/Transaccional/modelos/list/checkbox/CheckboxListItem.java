package Transaccional.modelos.list.checkbox;

import java.io.Serializable;

import lombok.Data;
	
@Data
public class CheckboxListItem implements Serializable, Cloneable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -3915046488123169010L;
	private int id;
	private String label;
	private boolean isSelected = false;
	private Object object;

	
	
	public CheckboxListItem(CheckboxListItem obj) {
		this.id = obj.id;
		this.label = new String(obj.label);
		this.isSelected = obj.isSelected;	
	}

	public CheckboxListItem(String label) {
		this.label = label;
	}

	public CheckboxListItem(int id,String label) {
		this.id = id;
		this.label = label;
	}
	
	public CheckboxListItem(int id, String label, Object object) {
		this.id = id;
		this.label = label;
		this.object = object;
	}
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String toString() {
		return label;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}


}
