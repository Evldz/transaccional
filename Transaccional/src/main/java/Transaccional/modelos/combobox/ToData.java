package Transaccional.modelos.combobox;

public class ToData {
	
	private int id;
	private String Descripcion;
	
	
	
	public ToData() {
		super();
	}



	public ToData(int id, String descripcion) {
		super();
		this.id = id;
		Descripcion = descripcion;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getDescripcion() {
		return Descripcion;
	}



	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}



	@Override
	public String toString() {
		return getDescripcion();
	}
	
	

}
