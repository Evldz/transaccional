package genera_llave;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.fasterxml.jackson.databind.ObjectMapper;

import genera_llave.crypto.RSAAsymetricCrypto;
import genera_llave.crypto.Usuario;

public class GeneraLlave {

	private JFrame frmGeneradorLlavesTransaccional;
	private JTextField txtGeneraLlavePublica;
	private JTextField txtGeneraLlavePrivada;
	private JTextField txtGeneraKey;
	private JPasswordField txtPassword;
	private JTextField txtLeePublica;
	private JTextField txtUsuario;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
					GeneraLlave window = new GeneraLlave();
					window.frmGeneradorLlavesTransaccional.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GeneraLlave() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGeneradorLlavesTransaccional = new JFrame();
		frmGeneradorLlavesTransaccional.setTitle("Generador de llaves Transaccional");
		frmGeneradorLlavesTransaccional.setResizable(false);
		frmGeneradorLlavesTransaccional.setBounds(100, 100, 647, 453);
		frmGeneradorLlavesTransaccional.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGeneradorLlavesTransaccional.getContentPane().setLayout(null);
		
		
		frmGeneradorLlavesTransaccional.setIconImage(
				(new ImageIcon(getClass().getResource("/genera_llave/imagenes/cripto2.png"))).getImage());
		
		JPanel pnlLlave = new JPanel();
		pnlLlave.setBorder(new TitledBorder(null, "Llaves", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlLlave.setBounds(6, 6, 629, 155);
		frmGeneradorLlavesTransaccional.getContentPane().add(pnlLlave);
		pnlLlave.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Pública");
		lblNewLabel.setBounds(10, 21, 239, 13);
		pnlLlave.add(lblNewLabel);
		
		txtGeneraLlavePublica = new JTextField();
		txtGeneraLlavePublica.setBounds(10, 35, 511, 28);
		pnlLlave.add(txtGeneraLlavePublica);
		txtGeneraLlavePublica.setColumns(10);
		
		JButton btnGeneraPublica = new JButton("...");
		btnGeneraPublica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File file=seleccionaArchivo();
				if(file!=null) {txtGeneraLlavePublica.setText(file.getAbsolutePath());}
			}
		});
		btnGeneraPublica.setBounds(533, 34, 85, 28);
		pnlLlave.add(btnGeneraPublica);
		
		JLabel lblNewLabel_1 = new JLabel("Privada");
		lblNewLabel_1.setBounds(10, 64, 206, 13);
		pnlLlave.add(lblNewLabel_1);
		
		txtGeneraLlavePrivada = new JTextField();
		txtGeneraLlavePrivada.setBounds(10, 81, 511, 28);
		pnlLlave.add(txtGeneraLlavePrivada);
		txtGeneraLlavePrivada.setColumns(10);
		
		JButton btnGeneraPrivada = new JButton("...");
		btnGeneraPrivada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File file=seleccionaArchivo();
				if(file!=null) {txtGeneraLlavePrivada.setText(file.getAbsolutePath());}

			}
		});
		btnGeneraPrivada.setBounds(533, 80, 85, 28);
		pnlLlave.add(btnGeneraPrivada);
		
		JButton btnGeneraLlaves = new JButton("Generar");
		btnGeneraLlaves.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File llavePublica = new File(txtGeneraLlavePublica.getText());
				File llavePrivada = new File(txtGeneraLlavePrivada.getText());
				
			
				if(txtGeneraLlavePrivada.getText().length() == 0 || txtGeneraLlavePublica.getText().length() == 0) {
					JOptionPane.showMessageDialog(null, "Debe seleccionar donde se va generar los archivos de llaves privadas y públicas");
					return;
				}
					
				
				if(llavePublica.exists()) {
					int result=JOptionPane.showConfirmDialog(null, "El archivo ".concat(llavePublica.getAbsolutePath()).concat(" sera borrado,¿Esta seguro?"));
					if(result==0) {
						llavePublica.delete();
					}
					else {
						return;
					}
				}
				
				if(llavePrivada.exists()) {
					int result=JOptionPane.showConfirmDialog(null, "El archivo ".concat(llavePrivada.getAbsolutePath()).concat(" sera borrado,¿Esta seguro?"));
					if(result==0) {
						llavePrivada.delete();
					}
					else {
						return;
					}

				}
				try {
					
					pnlLlave.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					RSAAsymetricCrypto.GeneraLlaves(llavePublica, llavePrivada);
					pnlLlave.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(null, "Llaves generadas exitosamente");
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				
			}
		});
		btnGeneraLlaves.setBounds(533, 117, 85, 28);
		pnlLlave.add(btnGeneraLlaves);
		
		JPanel pnlPass = new JPanel();
		pnlPass.setLayout(null);
		pnlPass.setBorder(new TitledBorder(null, "Contrase\u00F1a", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(59, 59, 59)));
		pnlPass.setBounds(6, 181, 629, 227);
		frmGeneradorLlavesTransaccional.getContentPane().add(pnlPass);
		
		JLabel lblArchivoKey = new JLabel("Archivo Key");
		lblArchivoKey.setBounds(6, 67, 142, 13);
		pnlPass.add(lblArchivoKey);
		
		txtGeneraKey = new JTextField();
		txtGeneraKey.setColumns(10);
		txtGeneraKey.setBounds(6, 81, 511, 28);
		pnlPass.add(txtGeneraKey);
		
		JButton btnNewButton_3_2 = new JButton("...");
		btnNewButton_3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file=seleccionaArchivo();
				if(file!=null) {txtGeneraKey.setText(file.getAbsolutePath());}				
			}
		});
		btnNewButton_3_2.setBounds(529, 80, 85, 28);
		pnlPass.add(btnNewButton_3_2);
		
		JLabel lblNewLabel_1_1 = new JLabel("Contraseña");
		lblNewLabel_1_1.setBounds(6, 172, 209, 13);
		pnlPass.add(lblNewLabel_1_1);
		
		JButton btnNewButton_3_1_1_1 = new JButton("Generar");
		btnNewButton_3_1_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File llaveKey = new File(txtGeneraKey.getText());
				File llavePublica  = new File(txtLeePublica.getText());
				
				if(txtLeePublica.getText().length() == 0 || txtGeneraKey.getText().length() == 0 || txtUsuario.getText().length() == 0 || txtPassword.getPassword().length == 0) {
					JOptionPane.showMessageDialog(null, "Es necesario indicar donde se va generar el archivo key,la llave pública a leer, el usuario y la contraseña");
					return;
				}
				
				if(txtUsuario.getText().length() > 110 || txtPassword.getPassword().length > 110 ) {
					JOptionPane.showMessageDialog(null, "El número máximo de caracteres que se permite en los campos  usuario y contraseña, es de 110 caracteres");
					return;					
				}
				
				if(!llavePublica.exists()) {
					JOptionPane.showMessageDialog(null, "Seleccione otra ubicación del archivo publico, la aplicación no puede acceder a ella");
					return;
				}
				
				if(llaveKey.exists()) {
					int result=JOptionPane.showConfirmDialog(null, "El archivo ".concat(llaveKey.getAbsolutePath()).concat(" sera borrado,¿Esta seguro?"));
					if(result==0) {
						llaveKey.delete();
					}
					else {
						return;
					}
				}
				
				
				try {
					
					pnlPass.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					Usuario usuario = new Usuario(txtUsuario.getText(), new String(txtPassword.getPassword()));
					RSAAsymetricCrypto.Encripta(new ObjectMapper().writeValueAsString(usuario), llavePublica, llaveKey);
					pnlPass.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
					JOptionPane.showMessageDialog(null, "Constraseña generada exitosamente");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnNewButton_3_1_1_1.setBounds(529, 134, 85, 28);
		pnlPass.add(btnNewButton_3_1_1_1);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(6, 185, 511, 28);
		pnlPass.add(txtPassword);
		
		JLabel lblPublica = new JLabel("Llave Pública a utilizar");
		lblPublica.setBounds(6, 17, 237, 13);
		pnlPass.add(lblPublica);
		
		txtLeePublica = new JTextField();
		txtLeePublica.setColumns(10);
		txtLeePublica.setBounds(6, 31, 511, 28);
		pnlPass.add(txtLeePublica);
		
		JButton btnNewButton_3_2_1 = new JButton("...");
		btnNewButton_3_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file=seleccionaArchivo();
				if(file!=null) {txtLeePublica.setText(file.getAbsolutePath());}
			}
		});
		btnNewButton_3_2_1.setBounds(529, 30, 85, 28);
		pnlPass.add(btnNewButton_3_2_1);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(6, 120, 142, 13);
		pnlPass.add(lblUsuario);
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		txtUsuario.setBounds(6, 134, 511, 28);
		pnlPass.add(txtUsuario);

	}
	
	private File seleccionaArchivo() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.showOpenDialog(null);
		File file = fileChooser.getSelectedFile();
		if (file != null) {
			int opcion = JOptionPane.showConfirmDialog(null,
					"¿Esta seguro que se genere la llave en  ".concat(file.getAbsolutePath()).concat("?"));
			if (opcion != 0)
				file = null;

		}
		return file;

	}
	
}
