package genera_llave.crypto;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Usuario implements Serializable {


	private static final long serialVersionUID = -5173684419625528591L;
	private String usuario;
	private String password;
	public Usuario(String usuario, String password) {
		this.usuario = usuario;
		this.password = password;
	}
	
}
